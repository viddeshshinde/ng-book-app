/**
 * Created by VIDDESH on 24/11/15.
 */

var myApp = angular.module('myApp',[]);

myApp.controller('MyController1',function($scope) {
    $scope.name = "Vid";
    $scope.clock = {
        now : new Date()
    }
    var updateClock = function() {
        $scope.clock.now = new Date();
    };
    setInterval(function() { $scope.$apply(updateClock);
    }, 1000);
    updateClock();
});


myApp.controller('ClickControllers',function($scope) {
    $scope.counter = 0;
    $scope.add = function(counter) {$scope.counter += counter;};
    $scope.sub = function(counter) {$scope.counter -= counter;};
});


myApp.controller('WatchControllers',function($scope,$parse) {
    $scope.parsedValue = 0;
    $scope.$watch('expr',function(newVal, oldVal, scope) {
        if(newVal !== oldVal) {
            var parseFun = $parse(newVal);
            $scope.parsedValue = parseFun(scope);
        }
    });
    $scope.add = function(counter) {$scope.counter += counter;};
    $scope.sub = function(counter) {$scope.counter -= counter;};
});


